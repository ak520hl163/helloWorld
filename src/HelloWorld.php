<?php
namespace HelloWorld;

class HelloWorld {

    private $author;

    public function __construct($author) 
    {
        $this->author = $author;
    }

    public function echo() 
    {
        $info = "Hello World!\n";
        $info .= "Power By ---$this->author---\n";

        return $info;
    }
}